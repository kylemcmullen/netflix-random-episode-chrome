// Netflix Random Episode

var seasonsData = [],
    loadingDone = false;

var randomButton = $('<span class="btnWrap mltBtn mltBtn-s60" />')
                    .append($('<a id="randomButton" href="#" class="svf-button svfb-blue addlk" />')
                        .append('<span class="inr">Play Random Episode</span>'));

var randomBOB = $('<div id="randomBOB" class="episodeBOB densityEpisodeBOB" style="top: -66px; visibility: visible; display: block; " />')
                    .append('<span class="ebob-arrow transp nse" />')
                    .append('<div class="ebob-head transp" />')
                    .append('<div class="ebob-content transp" />')
                    .append('<div class="ebob-foot transp" />');

var loadingScr = $('<div class="ebob-content transp" />')
                    .append('<h3 class="heading"> Loading Episodes </h3>')
                    .append('<div class="randomLoading" />');
                    
var loadingBOB = $('<div />')
                    .append(loadingScr)
                    .append('<a href="#" />');

// Adds a prefix to console log entries.
var orig = console.log
console.log = function(input) {
    orig.apply(console, ["Netflix Random Episode: ", input]);
}

// Replaces the currently selected episode with a new selection from the seasonsData global
getRandomEpisode = function() {
    // return if called before the episodes are finished loading
    if(!loadingDone) {
        return;
    }

    var episode = seasonsData[Math.floor(Math.random() * seasonsData.length)];
    
    console.log("Picked S" + episode.seasonNumber + "E" + episode.number + ".");

    // clone the randomBOB as a template
    var episodeBOB = randomBOB.clone();
    var episodeContent = episodeBOB.find(".ebob-content").html("");

    // episode number, season number, and duration in the heading
    var episodeNumber = episode.seasonNumber ? "Season " + episode.seasonNumber + ": ": "";
    episodeNumber += "Episode " + episode.number;
    var episodeTitle = $('<h3 class="heading">' + episodeNumber + '</h3>');

    if(episode.length) {
        episodeTitle.append('<span class="duration"><span>' + episode.length + '</span></span>');
    }
    
    episodeContent.append(episodeTitle);

    // add episode title if it has one
    if( episode.title != "Episode " + episode.number) {   
        episodeContent.append('<span class="title">' + episode.title + '</span>');
    } 
    
    // add synopsis (description)
    episodeContent.append('<p class="synopsis">' + episode.description + '</p>');

    // build a link to the episode
    episodeBOB.prepend('<a href="http://movies.netflix.com/WiPlayer?movieid=' + episode.id + '" />');

    replaceBob(episodeBOB);
}

// Parse seasons using pathConfig and return list containing an object for each season
parseSeasons = function(pathConfig) {
    var seasons = [];

    $(pathConfig.seasonPath).each(function(i,season) {
        var seasonIdElement = pathConfig.seasonIdPath ? $(season).find(pathConfig.seasonIdPath) : $(season);
        var seasonNumElement = pathConfig.seasonNumPath ? $(season).find(pathConfig.seasonNumPath) : $(season);

        seasons.push({
            "id": seasonIdElement.attr(pathConfig.seasonIdAttr),
            "number": seasonNumElement.html()
        });
    });
    
    if(seasons.length) {
        console.log("Found " + seasons.length + " seasons.");
    } else {
        console.log("Season selector not found.");
    }

    return seasons;
}

// Parse episodes in the episodeContainer using pathConfig and return list containing an object for each episode
parseEpisodes = function(seasonId, seasonNumber, episodeContainer, pathConfig) {
    if(!seasonNumber) {
        seasonNumber = 1;
    }

    var episodes = [];

    episodeContainer.find(pathConfig.episodePath).each(function(i, episode) {
        var episodeIdElement = pathConfig.episodeIdPath ? $(episode).find(pathConfig.episodeIdPath) : $(episode);

        console.log(episodeIdElement);

        episodes.push({
            "id": episodeIdElement.attr(pathConfig.episodeIdAttr),
            "number": $(episode).find(pathConfig.episodeNumberPath).html(),
            "seasonId": seasonId,
            "seasonNumber": seasonNumber,
            "length": $(episode).find(pathConfig.episodeLengthPath).html(),
            "progress": $(episode).find(pathConfig.episodeProgressPath).html(),
            "title": $(episode).find(pathConfig.episodeTitlePath).html(),
            "description": $(episode).find(pathConfig.episodeDescriptionPath).html()
        });
    });
    
    console.log("Parsed " + episodes.length + " episodes in season " + seasonNumber + ".");

    return episodes;
}

// Loads a list of DOM elements containing episodes into the seasonsData global.
loadEpisodes = function(pathConfig) {
    console.log("Loading episodes...");

    var seasons = parseSeasons(pathConfig);
    
    replaceBob(loadingBOB);
    
    if(seasons.length > 1) {
        console.log("Requesting data for multiple seasons.");

        var numLoaded = 0;

        $(seasons).each(function() {
            var season = this;
            var seasonURL = window.location.href.replace("#","") 
                + "&actionMethod=seasonDetails&seasonId=" 
                + season.id + "&seasonKind=ELECTRONIC";

            console.log("-> Season " + season.number + " [" + seasonURL + "]");
            
            $.getJSON(seasonURL, function(data) {
                // add the episodes in the response data to seasonsData
                var episodes = parseEpisodes(season.id, season.number, $(data.html), pathConfig);
                
                for(var e in episodes) {
                    seasonsData.push(episodes[e]);
                }
                
                numLoaded += 1;
                
                console.log("---> Finished loading season " + numLoaded + " of " + seasons.length);
                console.log("---> " + seasonsData.length + " episodes loaded so far.");
                
                // if this is the last async request, mark loading done
                if(numLoaded == seasons.length) {
                    loadingDone = true;
                }
            });
        });
    } else {
        seasonsData = parseEpisodes("", "", $(pathConfig.episodeContainer), pathConfig);
        loadingDone = true;
    }

    console.log("Loading finished.");
}

// Replaces the randomBOB and randomButton globals with content from newBOB.
replaceBob = function(newBOB) {
    var newContent = newBOB.find('.ebob-content').html();
    var newURL = newBOB.find('a').attr('href');
    
    randomBOB.find('.ebob-content').html('').append(newContent);
    randomButton.find('a').attr('href', newURL);
}

$(function() {
    // set up random episode button hover
    randomButton.hover(function(e) {
        getRandomEpisode();
        randomBOB.show();
    }, function(e) {
        randomBOB.hide();
    });
        
    // detect episodes and page type
    if($("#seasonDetail .episodeList li").length > 1) {
        console.log("Standard series page detected.");

        // append bob and button
        $("#displaypage-overview-details .actions").append(randomButton.append(randomBOB.hide()));
        
        loadEpisodes({
            "seasonPath": "#seasonsNav .seasonItem",
            "seasonIdPath": "a",
            "seasonIdAttr": "data-vid",
            "seasonNumPath": "a",
            "episodeContainer": "#seasonDetail",
            "episodePath": ".episodeList li",
            "episodeIdPath": "",
            "episodeIdAttr": "data-episodeid",
            "episodeNumberPath": ".seqNum",
            "episodeLengthPath": ".episodeBOB .ebob-content .heading .duration",
            "episodeProgressPath": ".episodeBOB .ebob-content .progress-bar-wrapper label:first",
            "episodeTitlePath": ".episodeTitle",
            "episodeDescriptionPath": ".episodeBOB .ebob-content .synopsis"
        });
    
    } else if($(".episodesContent .videoSeason .episode")) {
        console.log("Netflix Original Series page detected.");
        console.log("The extension does not support Netflix Original Series in this version.");

        /*
        // append bob and button
        $(".ShowInfo .moduleContent .showColumns .bobData").append(randomButton.append(randomBOB.hide()));

        loadEpisodes({
            "seasonPath": "", // TODO: add this when there's a Netflix Original with multiple seasons
            "seasonIdPath": "",
            "seasonIdAttr": "",
            "seasonNumPath": "",
            "episodeContainer": "#originalsDisplayBody .episodesContent",
            "episodePath": ".videoSeason .episode",
            "episodeIdPath": ".videoImagery",
            "episodeIdAttr": "data-episode-id",
            "episodeNumberPath": ".videoDetails .title .episodeNumber",
            "episodeLengthPath": ".videoDetails .progress-details .total-time .time-text",
            "episodeProgressPath": ".videoDetails .progress-details .cur-position .time-text",
            "episodeTitlePath": ".videoDetails .title .title-text",
            "episodeDescriptionPath": ".videoDetails .synopsis"
        });
        */
    } else {
        console.log("Page does not contain episodes.");
    }
    
});
